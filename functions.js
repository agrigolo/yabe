const yaml = require('js-yaml')
const config = require('./conf')
const path = require('path')
const fs = require('fs')


//Prototype utility function to replace all occurrences of a string
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

module.exports = {
    sort_by: (field, reverse, primer) => {

        var key = primer ?
            function (x) { return primer(x[field]) } :
            function (x) { return x[field] }

        reverse = !reverse ? 1 : -1

        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a))
        }
    },
    //Returns an object literal containing the YAML front matter parameters
    //TODO : More robust parsing of frontmatter to enable arbitrary length of separators (---)
    //separators have to be the same length by specifications
    frontMatter: (text) => {
        var frontMatter = text.split('---')[1]
        var metadata = yaml.safeLoad(frontMatter)
        //console.log(JSON.stringify(metadata))
        return metadata
    },

    //Returns the body of the Markdown document, excluding the YAML Front Matter
    mdBody: (text) => {
        mdArray = text.split('---')
        var mdBody = mdArray.slice(2).join('---')
        return mdBody
    },

    buildIndex: (callback) => {

        var posts = []
        var postFileNames = []
        var drafts = 0
        const postsFolder = config.postsDirectory
        //Populates an array with all the filenames in the posts directory
        fs.readdir(postsFolder, (err, files) => {
            files.forEach((file) => {
                postFileNames.push(file)
            })
            //The array is put in descending alphabetical order
            postFileNames.forEach((filename, index) => {

                var filepath = path.join(config.postsDirectory, filename)
                //Reads each post file and populates the object to be returned in the callback
                fs.readFile(filepath, 'utf8', (err, data) => {
                    if (err) {
                        callback('ERROR')
                    }
                    const mdData = data.toString(data)

                    //Gets the metadata object from the YAML frontmatter
                    var metadata = module.exports.frontMatter(mdData)
                    if (metadata.draft == false) {
                        metadata.filename = filename.split('.')[0]
                        posts.push(metadata)
                    }
                    //When the end of the array is reached, the callback is called
                    if (index + 1 === postFileNames.length) {
                        callback(posts)
                    }
                })
            })
        })
    }
}