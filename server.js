// server.js

const config = require('./conf')
const functions = require('./functions')
const express = require('express')
const app = express()
const path = require('path')
const fs = require('fs')
const yaml = require('js-yaml')
const pug = require('pug')
const chokidar = require('chokidar');

const md = require('markdown-it')({
    html: config.html,
    linkify: config.linkify,
    typographer: config.typographer
})

const emoji = require('markdown-it-emoji')
if (config.emoji === true) { md.use(emoji) }

app.set('view engine', 'pug')

//Builds the posts index at startup
var index = {}

var rebuildIndexWrapper = function () {
    functions.buildIndex((response) => {
        if (response === 'ERROR') { res.sendStatus(500) }
        index = response.sort(functions.sort_by('date', true, function (a) { return a.toUpperCase() }))
        console.log(new Date().toLocaleString() + ' - Index built')
    })
}

rebuildIndexWrapper()

////FS WATCHER
var watcher = chokidar.watch('posts', {
    ignored: /(^|[\/\\])\../,
    persistent: true,
    ignoreInitial: true
})

//Event listeners
watcher
    .on('add', path => {
        console.log(`File ${path} has been added, rebuilding index...`)
        rebuildIndexWrapper()
    })
    .on('change', path => {
        console.log(`File ${path} has been changed, rebuilding index...`)
        rebuildIndexWrapper()
    })
    .on('unlink', path => {
        console.log(`File ${path} has been deleted, rebuilding index...`)
        rebuildIndexWrapper()
    })

////

var port = process.env.PORT || config.port       // set port

// ROUTES
// =================================================================================
var router = express.Router()              // get an instance of the express Router

//Sends a rendered HTML page from a Markdown file
//The filename is the page id passed in the url
router.get('/post/:postId', (req, res) => {
    const pageFileName = path.join(config.postsDirectory, req.params.postId + ".md")
    fs.readFile(pageFileName, 'utf8', (err, data) => {
        if (err) {
            console.log('Post ' + req.params.postId + ' not found!')
            res.sendStatus(404)
            return false
        }
        const mdData = data.toString()

        //gets the metadata object from the YAML frontmatter
        var post = functions.frontMatter(mdData)
        post.content = md.render(functions.mdBody(mdData))
            //Adds the TABLE class to all html tables to allow Bootstrap rendering
            .replaceAll('<table>', '<table class="table table-striped">')

        //Renders the post page
        res.render('post', { post });
        return true
    })
})

//Index page
router.get('/', (req, res) => {
    //Renders the page
    const data = index
    res.render('index', { data });
    return true
})

//Gets the page specified in the querystring
router.get('/:pageId', (req, res) => {
    //Renders the page
    res.render(req.params.pageId, {});
    return true
})

//Get posts list by tag
router.get('/tag/:tag', (req, res) => {
    const tag = req.params.tag

    //Returns the posts for which the tag is found in the tags field
    var data = index.filter((post) => {
        if (post.tags) {
            return (post.tags.indexOf(tag) >= 0)
        }
    })
    res.render('tag', { data, tagName: tag });
    return true
})

// END ROUTES
// ================================================================================

// REGISTER ROUTES -------------------------------
app.use('/', router)

//Generic error handling
app.use(function (err, req, res, next) {
    console.error(err.stack)
    res.sendStatus(404)
})

// START THE SERVER
// ================================================================================
app.listen(port)
console.log('YABE - Yet Another Blog Server ' + config.version + ' started on port ' + port)