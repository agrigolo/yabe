YABE - Yet Another Blog Engine
==================================

Yabe is a very simple Node.js blog engine I wrote in a weekend for fun.
It is not a static site generator, pages are rendered server-side using Express and PUG as a template engine.

Yabe provides a few routes :

- An index page (/)
- A generic page (/[pageName])
- A post page (/posts/[postId])
- A page that shows posts filtered by tag (/tag/[tagId])

Yabe listens to changes in the "posts" folder on the filesystem so that when a post is added, updated or deleted, the index page data is updated accordingly and cached in memory.

Page templates are located in the "views" folder and are written in PUG.
Post files are written in Markdown and metadata is specified in a YAML frontmatter. 
Title, Author, Date, Description and an arbitrary number of Tags can be specified. Post information is shown in the index and post pages. The DRAFT parameter specifies if the post is visible or not.
See the example posts for reference.

Yabe is shipped with a very minimal and simple theme that can be customised by editing the /themes/post.css file.
