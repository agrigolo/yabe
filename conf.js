module.exports = {

    version: '0.1',
    emoji: true,
    port: '8080',
    postsDirectory: 'posts',

    //Markdown rendering options
    html: true,
    linkify: true,
    typographer: true

}